setlocal

rm build/*

set "options=-mmcs51 --callee-saves uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000"
sdcc %options% -c -o build/util.rel util.c

sdcc %options% -c -o build/uart.rel uart.c

sdcc %options% -c -o build/xram.rel xram.c

sdar -rc build/mylib.lib build/util.rel build/uart.rel build/xram.rel

sdcc %options% -o build/main.ihx main.c mylib.lib -L build

packihx build/main.ihx > build/main.hex

sdcc -mmcs51  --code-loc 0x2000 --opt-code-size --model-small -o build/tetris52.ihx tetris52.c
packihx build/tetris52.ihx > build/tetris52.hex
tools\hex2bin build\tetris52.hex


sdcc -mmcs51  --code-loc 0x2000 --opt-code-size --model-small -o build/test.ihx test.c build/util.rel
packihx build/test.ihx > build/test.hex
tools\hex2bin build\test.hex


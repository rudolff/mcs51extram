#include <8052.h>

#define XRAM_PAGE P1_0
#define XRAM_ENABLED P1_1

#define XRAM_START 128
#define XRAM_ROM_START 0x2000

enum {
  CMD_ID = 1,
  CMD_TEST,
  CMD_LOAD, // uint addr, uchar length, uchar [] data
  CMD_DUMP, // uint addr, uchar length
  CMD_JUMP, // uint addr
  CMD_COPY, // uint srcaddr, uint dstaddr, uint length, uchar pages
  CMD_SETPAGE,  // uchar page
  CMD_GETPAGE  // uchar page


};

void delay(unsigned int );
void uart_init();
unsigned char uart_receive ( );
unsigned int uart_receive_uint ( );
void uart_send(unsigned char rec);
void uart_send_string(unsigned char * str);
void xram_setpage(unsigned char num);
unsigned char xram_getpage();
void xram_check();
void xram_find_iram();
void xram_copy(unsigned int src, unsigned int dst, unsigned int size, unsigned char pages);

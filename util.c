#include "util.h"

unsigned char to_hex(unsigned char x)
{
  return x >= 10 ? (x-10) + 'A' : x + '0';
}


void delay(unsigned int mstim)
{
  unsigned int i,j;
  for(i=0;i<mstim;i++)
    for(j=0;j<500;j++);
}

/* Calculating XMODEM CRC-16 in 'C'
   ================================
   Reference model for the translated code */

#define CRC_POLYNOM 0x1021

/* On entry, addr=>start of data
             num = length of data
             crc = incoming CRC     */
unsigned int crc16(__xdata unsigned char *addr, unsigned int num, unsigned int crc)
{
  unsigned int i;

  for (; num>0; num--)               /* Step through bytes in memory */
  {
    crc = crc ^ (*addr++ << 8);      /* Fetch byte from memory, XOR into CRC top byte*/
    for (i=0; i<8; i++)              /* Prepare to rotate 8 bits */
    {
      if (crc & 0x8000)             /* bit 15 was set (now bit 16)... */
      {
        crc = ((crc << 1) ^ CRC_POLYNOM); /* XOR with XMODEM polynomic */
      }
      else
      {
        crc = crc << 1;                /* rotate */
      }  
    }
  }                                /* Loop until num=0 */
  return(crc);                     /* Return updated CRC */
}



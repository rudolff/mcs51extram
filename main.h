#include <8052.h>

#define SECTIONS_COUNT 16
#define SECTION_NAME_LENGTH 10


enum {
  SECTION_TYPE_EMPTY,
  SECTION_TYPE_CODE,
  SECTION_TYPE_DATA 
};

typedef struct {
  unsigned char type;
  unsigned char page;
  unsigned int addr;
  unsigned int length;
  unsigned int checksum;
  unsigned char name[SECTION_NAME_LENGTH];

  
} section_info;

__xdata __at (0x1f00) section_info sections[SECTIONS_COUNT];

enum {
  CMD_ID = 1,
  CMD_TEST,
  CMD_LOAD, // uint addr, uint length, uint checksum, uchar [] data
  CMD_FILL, // uint addr, uint length, uchar data_length, uchar [] data
  CMD_DUMP, // uint addr, uint length
  
  CMD_DUMP_ROM, // uint addr, uint length
  CMD_JUMP, // uint addr
  CMD_COPY, // uint srcaddr, uint dstaddr, uint length, uchar pages[7:4 src,3:0 dst]
  CMD_SETPAGE,  // uchar page, 7 bit - disable xram
  CMD_GETPAGE,  // uchar page
  
  CMD_XRAM,   // uchar enabled
  CMD_XRAM_SECTIONS

};


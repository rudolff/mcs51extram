#ifndef __XRAM_H__
#define __XRAM_H__

#include <8052.h>
#include "uart.h"

#define XRAM_PAGE P1_0
#define XRAM_DISABLED P1_1

#define XRAM_START 256 // Some part of xram can be located on the MCU chip 
#define XRAM_ROM_START 0x2000 
#define XRAM_COMMON_ROM_PAGE 0
#define XRAM_PAGES_COUNT 2

void xram_init();
void xram_enabled(unsigned char enabled);
void xram_setpage(unsigned char num);
unsigned char xram_getpage();
void xram_fill(unsigned int start, unsigned int size, unsigned char data);
void xram_check();
void xram_find_iram();
void xram_find_common_rom();
void xram_copy(unsigned int src, unsigned int dst, unsigned int size, unsigned char pages);
void xram_copy_rom_to_ram(unsigned int src, unsigned int dst, unsigned int size, unsigned char dst_page);

#endif //__XRAM_H__

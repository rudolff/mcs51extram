
rm build/*

sdcc -mmcs51 --callee-saves drawbitmap,random,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -c -o build/util.rel util.c

sdcc -mmcs51 --callee-saves drawbitmap,random,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -c -o build/main_util.rel ../util.c


sdcc -mmcs51 --callee-saves drawbitmap,random,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -c -o build/glcd.rel glcd.c

sdcc -mmcs51 --callee-saves drawbitmap,random,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -c -o build/glcdfont.rel glcdfont.c

sdcc -mmcs51 --callee-saves drawbitmap,random,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -c -o build/uart.rel ../uart.c


sdar -rc build/glcdlib.lib build/util.rel build/glcd.rel build/glcdfont.rel build/uart.rel build/main_util.rel

sdcc --code-loc 0x2000 -mmcs51 --callee-saves drawbitmap,random,spiwrite,st7565_data,st7565_command,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -o build/main.ihx stlcd.c glcdlib.lib -L build

rem ssdcc -mmcs51 --callee-saves spiwrite,st7565_data,st7565_command,setpixel,uart_receive,uart_receive_uint,uart_send,uart_send_hex,uart_send_newline,uart_send_string,to_hex,xram_setpage --opt-code-size --model-small --xram-loc 0x100 --xram-size 0xFF00 --code-size 0x10000 -o build/main.ihx stlcd.c mylib.lib build/util.rel build/glcd.rel build/glcdfont.rel -L build -L ../build

packihx build/main.ihx > build/main.hex
..\tools\hex2bin build\main.hex


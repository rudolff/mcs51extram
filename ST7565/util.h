
#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif

#ifndef uint8_t 
#define uint8_t unsigned char
#endif

#ifndef int8_t 
#define int8_t signed char
#endif




// by default we stick strings in ROM to save RAM
#define nop __asm__ ("nop")

// some timing functions

void delay_10us(uint8_t us);
void _delay_ms(unsigned int mstim);
void delay_s(uint8_t s);

unsigned random();
void srandom(unsigned);
uint8_t reverse_byte(uint8_t c);



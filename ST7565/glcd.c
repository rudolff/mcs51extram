/*
$Id:$

ST7565 LCD library!

Copyright (C) 2010 Limor Fried, Adafruit Industries

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdlib.h>
#include "glcd.h"

#pragma callee_saves setpattern,clearpattern

extern __code uint8_t font[];
uint8_t font_size = FONT_SIZE_NORMAL;

// the most basic function, set a single pixel
/*void setpixel_old(__xdata uint8_t *buff, uint8_t x, uint8_t y, uint8_t color) {
  if ((x >= LCDWIDTH) || (y >= LCDHEIGHT))
    return;

  // x is which column
  if (color) 
    buff[x+ (y>>3)*128] |= _BV(7-(y&7));  
  else
    buff[x+ (y>>3)*128] &= ~_BV(7-(y&7)); 
}*/

void setpixel(__xdata uint8_t *buff, uint8_t x, uint8_t y, uint8_t color) {

  __asm
    mov	a,#0x100 - 0x80
    add	a,_setpixel_PARM_2
    jc	setpixel_exit
    mov	a,#0x100 - 0x40
    add	a,_setpixel_PARM_3
    jc	setpixel_exit

    mov a,_setpixel_PARM_3 // calculate address
    swap a
    mov b,a
    anl a,#0x80
    orl	a,_setpixel_PARM_2    
    add a,dpl
    mov dpl,a
    mov a,b
    anl a,#0x0F
    addc a,dph
    mov dph,a

    mov a,_setpixel_PARM_3
    anl a,#0x07
    
    add a, #(setpixel_mask-setpixel_mask_pc)
    movc a, @a+pc
setpixel_mask_pc: 

    mov b,a // save mask
    mov a,_setpixel_PARM_4 // 
    jz setpixel_zero // if clear

    movx a,@dptr
    orl a, b 
    movx @dptr,a
    
    ret 
       
setpixel_zero:
    mov a,b
    cpl a
    mov b,a
    movx a,@dptr
    anl a, b 
    movx @dptr,a 
       
setpixel_exit: 
    ret    
    
setpixel_mask:    
    .db 0x80
    .db 0x40
    .db 0x20
    .db 0x10
    .db 0x08
    .db 0x04
    .db 0x02
    .db 0x01
    
  __endasm;
}


void setpattern(__xdata uint8_t *buff, uint8_t x, uint8_t row, uint8_t pattern) {

  __asm
    mov	a,#0x100 - 0x80
    add	a,_setpattern_PARM_2
    jc	setpattern_exit
    mov	a,#0x100 - 0x08
    add	a,_setpattern_PARM_3
    jc	setpattern_exit

    mov a,_setpattern_PARM_3 // calculate address
    rr a
    mov b,a
    anl a,#0x80
    orl	a,_setpattern_PARM_2    
    add a,dpl
    mov dpl,a
    mov a,b
    anl a,#0x0F
    addc a,dph
    mov dph,a
    
    movx a,@dptr
    orl a, _setpattern_PARM_4 
    movx @dptr,a
         
setpattern_exit: 

  __endasm;
}

void clearpattern(__xdata uint8_t *buff, uint8_t x, uint8_t row, uint8_t pattern) {

  __asm
    mov	a,#0x100 - 0x80
    add	a,_clearpattern_PARM_2
    jc	clearpattern_exit
    mov	a,#0x100 - 0x08
    add	a,_clearpattern_PARM_3
    jc	clearpattern_exit

    mov a,_clearpattern_PARM_3 // calculate address
    rr a
    mov b,a
    anl a,#0x80
    orl	a,_clearpattern_PARM_2    
    add a,dpl
    mov dpl,a
    mov a,b
    anl a,#0x0F
    addc a,dph
    mov dph,a
    
    mov a, _clearpattern_PARM_4
    cpl a
    mov b,a
    
    movx a,@dptr
    anl a, b 
    movx @dptr,a
         
clearpattern_exit: 

  __endasm;
}

/*
  This function copies vertical bitmap (90 degree rotated) to screen area 

  For example:
  byte 0  byte 2
  7       7
  ..
  0       0        
  
  byte 1  byte 3
  7       7
  ..
  0       0

*/

void drawbitmap(__xdata uint8_t *buff, uint8_t x, uint8_t y, 
  const uint8_t * bitmap, uint8_t w, uint8_t h, uint8_t color) {
	uint8_t j, i, y2, tail, ptr = 0;
	
  if(h&7)
  {
    h += 7;
  }
  h >>= 3; // count bytes instead bits

  if(color)
  {
    for (j=0; j<w; j++) {
      y2 = (y&0x80 ? 0xE0 : 0) | (y>>3);
      for (i=0, tail = 0; i<h; i++, ptr++ ) {
      
        setpattern(buff, x+j, y2+i, (bitmap[ptr] >> (y&7)) | tail );
        if(y&7)
        {
          tail = bitmap[ptr] << (8-y&7);
        }
      }
      if(y&7)
      {
        setpattern(buff, x+j, y2+i, tail);
      }      
    }
  }
  else
  {
    for (j=0; j<w; j++) {
      y2 = (y&0x80 ? 0xE0 : 0) | (y>>3);
      for (i=0, tail = 0; i<h; i++, ptr++ ) {
        clearpattern(buff, x+j, y2+i, (bitmap[ptr] >> (y&7)) | tail );
        if(y&7)
        {
          tail = bitmap[ptr] << (8-y&7);
        }
      }
      if(y&7)
      {
        clearpattern(buff, x+j, y2+i, tail);
      }      
    }
  }  
}

/*
void drawbitmap(__xdata uint8_t *buff, uint8_t x, uint8_t y, 
  const uint8_t * bitmap, uint8_t w, uint8_t h, uint8_t color) {
	uint8_t j, i;
	
  for (j=0; j<h; j++) {
    for (i=0; i<w; i++ ) {
      if (bitmap[i + (j>>3)*w] & _BV(j&7)) {
        setpixel(buff, x+i, y+j, color);
      }
    }
  }
}


void drawbitmap_try(__xdata uint8_t *buff, uint8_t x, uint8_t y, 
  const uint8_t * bitmap, uint8_t w, uint8_t h, uint8_t color) {
	uint8_t j, i, data, isChanged = 0;
	__xdata uint8_t *index;
	
  for (i=0; i<w; i++ ) {
    index = buff + x + i + (y>>3)*128;
    
    for (j=0; j<h; j++) {
      
      if(!((y+j)&7))
      {
        if(isChanged)
        {
          *index = data;
          isChanged = 0;
        }
        
        index = buff + x + i + ((y+j)>>3)*128;
      }   
       
      if (bitmap[i + (j>>3)*w] & _BV(j&7)) {

        if(!isChanged)
        {
          data = *index;
        }
           
        if (color) 
          data |= _BV(7-((y+j)&7));  
        else
          data &= ~_BV(7-((y+j)&7)); 
        
        isChanged = 1;
      }
    }
    if(isChanged)
    {
      *index = data;
      isChanged = 0;
    }
  }


}*/

inline void setfontsize(uint8_t size) {
  font_size = size;
}

/*
void drawstring(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t *c) {
  uint8_t width = 6;
  while (c[0]) {
    switch(font_size)
    {
      case FONT_SIZE_2X:
        drawchar2x(buff, x, line, c[0]);
        width = 12;
        break;

      case FONT_SIZE_2H:
        drawchar2h(buff, x, line, c[0]);
        break;
        
      default:
        drawchar(buff, x, line, c[0]);
    }

    c++;
    x += width; // 6 pixels wide
    if (x + width >= LCDWIDTH) {
      x = 0;    // ran out of this line
      line++;
    }
    if (line >= (LCDHEIGHT/8))
      return;        // ran out of space :(
  }

}

void drawchar(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c) {
  uint8_t i;
  for (i =0; i<5; i++ ) {
    buff[x + (line*128) ] = font[(c*5)+i];
    x++;
  }
}

void drawchar_opt(__xdata uint8_t *buff, uint8_t x, uint8_t y, uint8_t c) {
  uint8_t i;
  unsigned int addr; 
  for (i =0; i<5; i++ ) {
    
    addr = x + ((y & ~7)<<4);
    if(addr < 1024)
    {
      buff[addr] = buff[addr] & ~((1<<(7 - (y&7)))-1) | (font[c*5 + i] >> (y&7));
    }
    addr += 128;
    if((y&7) && addr < 1024)
    {
      buff[addr] = buff[addr] & ((1<<(y&7))-1) | font[c*5 + i] << (8 - (y&7));
    } 
    x++;
  }
}


void drawchar2h(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c) {
  uint8_t i, t, b;
  for (i =0; i<5; i++ ) {
    t = font[(c*5)+i];
    t = (t&0x80) | ((t&0x80)>>1) | ((t&0x40)>>1) | ((t&0x40)>>2) 
        | ((t&0x20)>>2) | ((t&0x20)>>3)| ((t&0x10)>>3) | ((t&0x10)>>4);
        
    b =  font[(c*5)+i];    
    b = ((b&0x8)<<4) | ((b&0x8)<<3) | ((b&0x4)<<3) | ((b&0x4)<<2) 
        | ((b&0x2)<<2) | ((b&0x2)<<1)| ((b&0x1)<<1) | (b&0x1);

    buff[x + (line*128) ] = t;
    buff[x + ((line+1)*128) ] = b;
    x++;
  }
}

void drawchar2x(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c) {
  uint8_t i, t, b;
  for (i =0; i<5; i++ ) {
    t = font[(c*5)+i];
    t = (t&0x80) | ((t&0x80)>>1) | ((t&0x40)>>1) | ((t&0x40)>>2) 
        | ((t&0x20)>>2) | ((t&0x20)>>3)| ((t&0x10)>>3) | ((t&0x10)>>4);
        
    b =  font[(c*5)+i];    
    b = ((b&0x8)<<4) | ((b&0x8)<<3) | ((b&0x4)<<3) | ((b&0x4)<<2) 
        | ((b&0x2)<<2) | ((b&0x2)<<1)| ((b&0x1)<<1) | (b&0x1);

    buff[x + (line*128) ] = t;
    buff[x + ((line+1)*128) ] = b;
    x++;
    buff[x + (line*128) ] = t;
    buff[x + ((line+1)*128) ] = b;
    x++;
  }
}


// bresenham's algorithm - thx wikpedia
void drawline(__xdata uint8_t *buff,
	      uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, 
	      uint8_t color) {

  uint8_t dx, dy;
  int8_t err, ystep;
  uint8_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    swap(x0, y0);
    swap(x1, y1);
  }

  if (x0 > x1) {
    swap(x0, x1);
    swap(y0, y1);
  }

  dx = x1 - x0;
  dy = abs(y1 - y0);

  err = dx / 2;

  if (y0 < y1) {
    ystep = 1;
  } else {
    ystep = -1;}

  for (; x0<x1; x0++) {
    if (steep) {
      setpixel(buff, y0, x0, color);
    } else {
      setpixel(buff, x0, y0, color);
    }
    err -= dy;
    if (err < 0) {
      y0 += ystep;
      err += dx;
    }
  }
}

// filled rectangle
void fillrect(__xdata uint8_t *buff,
	      uint8_t x, uint8_t y, uint8_t w, uint8_t h, 
	      uint8_t color) {

  uint8_t i, j;
  // stupidest version - just pixels - but fast with internal buffer!
  for (i=x; i<x+w; i++) {
    for (j=y; j<y+h; j++) {
      setpixel(buff, i, j, color);
    }
  }
}


// draw a rectangle
void drawrect(__xdata uint8_t *buff,
	      uint8_t x, uint8_t y, uint8_t w, uint8_t h, 
	      uint8_t color) {
	      
  uint8_t i;
	      
  // stupidest version - just pixels - but fast with internal buffer!
  for (i=x; i<x+w; i++) {
    setpixel(buff, i, y, color);
    setpixel(buff, i, y+h-1, color);
  }
  for (i=y; i<y+h; i++) {
    setpixel(buff, x, i, color);
    setpixel(buff, x+w-1, i, color);
  } 
}
*/

// draw a circle
void drawcircle(__xdata uint8_t *buff,
	      uint8_t x0, uint8_t y0, uint8_t r, 
	      uint8_t color) {
  int8_t f = 1 - r;
  int8_t ddF_x = 1;
  int8_t ddF_y = -2 * r;
  int8_t x = 0;
  int8_t y = r;

  setpixel(buff, x0, y0+r, color);
  setpixel(buff, x0, y0-r, color);
  setpixel(buff, x0+r, y0, color);
  setpixel(buff, x0-r, y0, color);

  while (x<y) {
    if (f >= 0) {
      y--;
      ddF_y += 2;
      f += ddF_y;
    }
    x++;
    ddF_x += 2;
    f += ddF_x;
  
    setpixel(buff, x0 + x, y0 + y, color);
    setpixel(buff, x0 - x, y0 + y, color);
    setpixel(buff, x0 + x, y0 - y, color);
    setpixel(buff, x0 - x, y0 - y, color);
    
    setpixel(buff, x0 + y, y0 + x, color);
    setpixel(buff, x0 - y, y0 + x, color);
    setpixel(buff, x0 + y, y0 - x, color);
    setpixel(buff, x0 - y, y0 - x, color);
    
  }
}


// draw a circle
void fillcircle(__xdata uint8_t *buff,
	      uint8_t x0, uint8_t y0, uint8_t r, 
	      uint8_t color) {
  uint8_t i;
  int8_t f = 1 - r;
  int8_t ddF_x = 1;
  int8_t ddF_y = -2 * r;
  int8_t x = 0;
  int8_t y = r;

  for (i=y0-r; i<=y0+r; i++) {
    setpixel(buff, x0, i, color);
  }

  while (x<y) {
    if (f >= 0) {
      y--;
      ddF_y += 2;
      f += ddF_y;
    }
    x++;
    ddF_x += 2;
    f += ddF_x;
  
    for (i=y0-y; i<=y0+y; i++) {
      setpixel(buff, x0+x, i, color);
      setpixel(buff, x0-x, i, color);
    } 
    for (i=y0-x; i<=y0+x; i++) {
      setpixel(buff, x0+y, i, color);
      setpixel(buff, x0-y, i, color);
    }    
  }
}


// clear everything
void clear_buffer(__xdata uint8_t *buff) {
  unsigned i = 0;
  for(; i<1024; i++)
  {
    buff[i] = 0;
  }
}

// general purpose stuff, use as you'd like!

#include "util.h"



// Some basic delays...
void delay_10us(uint8_t ns)
{
  uint8_t i;

  while (ns != 0) {
    ns--;
    for (i=0; i< 30; i++) {
      nop;
    }
  }
}


void _delay_ms(unsigned int mstim)
{
  unsigned int i,j;
  for(i=0;i<mstim;i++)
    for(j=0;j<500;j++);
}

void delay_s(uint8_t s) {
  while (s--) {
    _delay_ms(1000);
  }
}


unsigned random()
{
  static unsigned int lfsr = 0xACE1u;
  unsigned int bit;
  

  bit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
  return lfsr =  (lfsr >> 1) | (bit << 15);
}

void srandom(unsigned val)
{

}

#define REVERSE_BYTE_MODE 0


uint8_t reverse_byte(uint8_t c) {

#if REVERSE_BYTE_MODE==4
// lookup for nibble, ~ 15 cycles
  __asm
    
    mov a,dpl
    anl a,#0xF

    add a, #(reverse_byte_lookup-reverse_byte_pc)
    movc a, @a+pc
reverse_byte_pc: 
    swap a    
    xch a,dpl
    swap a
    anl a,#0xF    

    add a, #(reverse_byte_lookup-reverse_byte_pc2)
    movc a, @a+pc
reverse_byte_pc2: 
    orl a,dpl
    mov dpl,a

    ret

reverse_byte_lookup:    
    .db 0x00
    .db 0x08
    .db 0x04
    .db 0x0C
    .db 0x02
    .db 0x0A
    .db 0x06
    .db 0x0E
    .db 0x01
    .db 0x09
    .db 0x05
    .db 0x0D
    .db 0x03
    .db 0x0B
    .db 0x07
    .db 0x0F    
  
  __endasm;


#elif REVERSE_BYTE_MODE==3
// ~18 cycles
  __asm
      mov b,dpl // 1
      mov c,b.0 // 1
      rlc a // 1
      mov c,b.1 
      rlc a 
      mov c,b.2 
      rlc a 
      mov c,b.3 
      rlc a 
      mov c,b.4 
      rlc a 
      mov c,b.5 
      rlc a 
      mov c,b.6 
      rlc a 
      mov c,b.7 
      rlc a 
     
      mov dpl,a 
  
  __endasm;

#elif REVERSE_BYTE_MODE==2
// constant rate 19 cycles
  __asm
      mov a,dpl
      swap a
      mov dpl,a
      
      rl a
      rl a
      anl a,#0xCC
      xch a,dpl

      rr a
      rr a
      anl a,#0x33
      orl a, dpl
      mov dpl,a
      
      rl a
      anl a,#0xAA
      xch a,dpl

      rr a
      anl a,#0x55
      orl a, dpl
      
      mov dpl,a
  __endasm;

#elif REVERSE_BYTE_MODE==1 
  uint8_t tmp, i = 8;
  
  while(i--)
  {
    tmp >>= 1;
    tmp |= c&0x80;
    c <<= 1;
  }
  
  return tmp;

#else

  c = ((c&0xF) << 4) | ((c&~0xF) >> 4);
  c = ((c&0x33) << 2) | ((c&~0x33) >> 2);
  c = ((c&0x55) << 1) | ((c&~0x55) >> 1);
  
  return c;
#endif
}







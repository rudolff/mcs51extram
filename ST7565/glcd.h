#define LCDWIDTH 128
#define LCDHEIGHT 64

#define _BV(bit) (1 << (bit)) 

#ifndef uint8_t 
#define uint8_t unsigned char
#endif

#ifndef int8_t 
#define int8_t signed char
#endif

enum {
  FONT_SIZE_NORMAL,
  FONT_SIZE_2H,
  FONT_SIZE_2X
};

#define swap(a, b) { uint8_t t = a; a = b; b = t; }
inline void setpixel(__xdata uint8_t *buff, uint8_t x, uint8_t y, uint8_t color);
void setpattern(__xdata uint8_t *buff, uint8_t x, uint8_t row, uint8_t pattern);
void clearpattern(__xdata uint8_t *buff, uint8_t x, uint8_t row, uint8_t pattern);

void drawrect(__xdata uint8_t *buff,
	      uint8_t x, uint8_t y, uint8_t w, uint8_t h, 
	      uint8_t color);
void drawline(__xdata uint8_t *buff,
	      uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, 
	      uint8_t color);


void drawbitmap(__xdata uint8_t *buff, uint8_t x, uint8_t y, 
		const uint8_t * bitmap, uint8_t w, uint8_t h,
		uint8_t color);
void drawchar(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c);
void drawchar2h(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c);
void drawchar2x(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t c);
void drawchar_opt(__xdata uint8_t *buff, uint8_t x, uint8_t y, uint8_t c);

void drawcircle(__xdata uint8_t *buff,
	      uint8_t x0, uint8_t y0, uint8_t r, 
		uint8_t color);

void fillrect(__xdata uint8_t *buff,
	      uint8_t x, uint8_t y, uint8_t w, uint8_t h, 
	      uint8_t color);

void setfontsize(uint8_t size);	      
void drawstring(__xdata uint8_t *buff, uint8_t x, uint8_t line, uint8_t *c);
	      

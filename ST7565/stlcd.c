/*
$Id:$

ST7565 LCD library!

Copyright (C) 2010 Limor Fried, Adafruit Industries

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 // some of this code was written by <cstone@pobox.com> originally; it is in the public domain.
*/

#include <stdlib.h>
#include "stlcd.h"
#include "glcd.h"
#include "util.h"
#include "../uart.h"

#define SID P1_2
#define SCLK P1_3
#define ASM_SID _P1_2
#define ASM_SCLK _P1_3
#define A0 P1_4
#define RST P1_5
#define CS P1_6
#define LED P1_7

#define XRAM_DISABLED P1_1


__code uint8_t logo16_glcd_bmp[]={
0x0F, 0xF0, 0x10, 0x08, 0x3F, 0xFC, 0x40, 0x02, 0xFF, 0xFF, 0x80, 0x01, 0xFF, 0xFF, 0x80, 0x01, 
0xFF, 0xFF, 0x80, 0x01, 0xFF, 0xFF, 0x40, 0x02, 0x3F, 0xFC, 0x10, 0x08, 0x0F, 0xF0, 0x00, 0x00, };

uint8_t is_reversed = 0;

__code uint8_t pagemap[] = { 7, 6, 5, 4, 3, 2, 1, 0 };

__xdata uint8_t buffer[1024];

void putstring_nl(unsigned char * str)
{
  uart_send_string(str);
  uart_send_newline();
}


void loop(void) {}

void setup(void) {
  XRAM_DISABLED = 0;

    uart_init();
    // UART MODE 0
    // SM0 = 0;
    // SM1 = 0;

    putstring_nl("init!");

    st7565_init();

    putstring_nl("on");
    st7565_command(CMD_DISPLAY_ON);

    putstring_nl("normal");
    st7565_command(CMD_SET_ALLPTS_NORMAL);

    putstring_nl("bright");
    st7565_set_brightness(0x0b);
    
    putstring_nl("clear screen");
    clear_buffer(buffer);
    clear_screen();


    LED = 1;
    

/*
    clear_buffer(buffer);
    setfontsize(FONT_SIZE_2X);

    drawstring(buffer, 2, 0, "Hi friend!");
    setfontsize(FONT_SIZE_2H);
    drawstring(buffer, 2, 2, "How are you?");
    write_buffer(buffer);
    setfontsize(FONT_SIZE_NORMAL);

    _delay_ms(2000);
    

    putstring_nl("corner points");
    clear_buffer(buffer);
    setpixel(buffer, 0, 0, 1);
    setpixel(buffer, 127, 0, 1);
    setpixel(buffer, 0, 63, 1);
    setpixel(buffer, 127, 63, 1);
    write_buffer(buffer);
    _delay_ms(2000);
    
    putstring_nl("draw char");
    clear_buffer(buffer);
    testdrawchar2x(buffer);
    write_buffer(buffer);
    _delay_ms(2500);

    putstring_nl("draw char");
    clear_buffer(buffer);
    testdrawchar2h(buffer);
    write_buffer(buffer);
    _delay_ms(2500);


    scroll_right(buffer, 0x01);
    scroll_right(buffer, 0x01);
    write_buffer(buffer);
    _delay_ms(2500);
    
    scroll_display(buffer, 64, 0x01);

    testdrawchar(buffer);
    write_buffer(buffer);
    _delay_ms(2500);
    
    scroll_display(buffer, 16, 0x10);

    testdrawchar(buffer);
    write_buffer(buffer);
    _delay_ms(2500);
    
    scroll_display(buffer, 128, 0x90);

    
    clear_buffer(buffer);
    drawrect(buffer, 10, 10, 20, 20, 1);
    write_buffer(buffer);
    _delay_ms(2500);
    putstring_nl("draw rect");
    clear_buffer(buffer);
    testdrawrect(buffer);
    write_buffer(buffer);
    _delay_ms(2500);
    putstring_nl("fill rect");
    clear_buffer(buffer);
    testfillrect(buffer);
    write_buffer(buffer);
    _delay_ms(2500);
    putstring_nl("draw line");
    clear_buffer(buffer);
    testdrawline(buffer);
    write_buffer(buffer);
    _delay_ms(2500);*/
    putstring_nl("draw circle");
    clear_buffer(buffer);
    testdrawcircle(buffer);
    write_buffer(buffer);

    _delay_ms(2500);
    clear_buffer(buffer);
    testdrawbitmap(buffer, logo16_glcd_bmp, 16, 16);

    LED = 0;
}


int main(void) {
  setup();
  while (1) {
    loop();
  }
}

#define NUMFLAKES 30
#define XPOS 0
#define YPOS 1
#define DELTAY 2

void testdrawbitmap(__xdata uint8_t *buff, const uint8_t *bitmap, uint8_t w, uint8_t h) {
  __xdata uint8_t icons[NUMFLAKES][3];
  uint8_t f;

  // initialize
  for (f=0; f< NUMFLAKES; f++) {
    icons[f][XPOS] = random() & 127;
    icons[f][YPOS] = 240;
    icons[f][DELTAY] = random() % 5 + 1;
  }

  while (1) {
    // draw each icon

    for (f=0; f< NUMFLAKES; f++) {
      drawbitmap(buffer, icons[f][XPOS], icons[f][YPOS], logo16_glcd_bmp, w, h, 1);
    }
    write_buffer(buffer);
    //_delay_ms(200);
    
    // then erase it + move it
    for (f=0; f< NUMFLAKES; f++) {
      drawbitmap(buffer, icons[f][XPOS], icons[f][YPOS],  logo16_glcd_bmp, w, h, 0);
      // move it
      icons[f][YPOS] += icons[f][DELTAY];
      // if its gone, reinit
      if (icons[f][YPOS] > 64 && icons[f][YPOS] < 240) {
        icons[f][XPOS] = random() & 127;
        icons[f][YPOS] = 240;
        icons[f][DELTAY] = random() % 5 + 1;
      }
    }
  }
}

void testdrawchar(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i < 126; i++) {
    drawchar_opt(buffer, (i % 21) * 6, (i/21)*11, i);
  }    
}

void testdrawchar2h(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i < 84; i++) {
    drawchar2h(buffer, (i % 21) * 6, (i/21)*2, i);
  }    
}

void testdrawchar2x(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i < 84; i++) {
    drawchar2x(buffer, (i % 10) * 12, (i/10)*2, i);
  }    
}


void testdrawcircle(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i<64; i+=2) {
    drawcircle(buffer, 63, 31, i, 1);
  }
}

void testdrawline(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i<128; i+=4) {
    drawline(buffer, 0, 0, i, 63, 1);
  }
  for (i=0; i<64; i+=4) {
    drawline(buffer, 0, 0, 127, i, 1);
  }

  write_buffer(buffer);
  _delay_ms(1000);

  for (i=0; i<128; i+=4) {
    drawline(buffer, i, 63, 0, 0, 0);
  }
  for (i=0; i<64; i+=4) {
    drawline(buffer, 127, i, 0, 0, 0);
  }
}

void testdrawrect(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i<32; i+=2) {
    drawrect(buff, i, i, 128-i*2, 64-i*2, 1);

  }
}


void testfillrect(__xdata uint8_t *buff) {
  uint8_t i;
  for (i=0; i<32; i++) {
    fillrect(buff, i, i, 128-i*2, 64-i*2, i%2);
    write_buffer(buffer);
    //_delay_ms(100);
  }
}

void clear_screen(void) {
  uint8_t p, c;
  
  for(p = 0, c = 0; p < 8; p++) {

    st7565_command(CMD_SET_PAGE | p);
    st7565_command(CMD_SET_COLUMN_LOWER | (c & 0xf));
    st7565_command(CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    for(c = 0; c < 128; c++) {

      st7565_data(0x00);
    }     
  }
}


void st7565_init(void) {
  // set pin directions

  
  // toggle RST low to reset; CS low so it'll listen to us
  CS = 0;
  RST = 0;
  _delay_ms(500);
  RST = 1;

  // LCD bias select
  st7565_command(CMD_SET_BIAS_9);
  // ADC select
  st7565_command(CMD_SET_ADC_NORMAL);
  // SHL select
  st7565_command(CMD_SET_COM_NORMAL);
  // Initial display line
  st7565_command(CMD_SET_DISP_START_LINE);

  // turn on voltage converter (VC=1, VR=0, VF=0)
  st7565_command(CMD_SET_POWER_CONTROL | 0x4);
  // wait for 50% rising
  _delay_ms(50);

  // turn on voltage regulator (VC=1, VR=1, VF=0)
  st7565_command(CMD_SET_POWER_CONTROL | 0x6);
  // wait >=50ms
  _delay_ms(50);

  // turn on voltage follower (VC=1, VR=1, VF=1)
  st7565_command(CMD_SET_POWER_CONTROL | 0x7);
  // wait
  _delay_ms(10);

  // set lcd operating voltage (regulator resistor, ref voltage resistor)
  st7565_command(CMD_SET_RESISTOR_RATIO | 0x6);

  // initial display line
  // set page address
  // set column address
  // write display data
}

#define SPIWRITE_ASM 1

void spiwrite(uint8_t c) {
#if SPIWRITE_ASM

  __asm
      mov a,dpl
      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

      clr ASM_SCLK
      rlc a
      mov ASM_SID,c
      setb ASM_SCLK

  __endasm;

#else

  int8_t i = 8;
  while (i) {
    --i;
    SCLK = 0;
    SID = (c & 0x80);
    c <<= 1;
    SCLK = 1;
  }
  
#endif  

}



inline void spiwrite_uart(uint8_t c) {
  uart_send(reverse_byte(c));
}


void st7565_command(uint8_t c) {
  A0 = 0;

  spiwrite(c);
}

void st7565_data(uint8_t c) {
  A0 = 1;

  spiwrite(c);
}
void st7565_set_brightness(uint8_t val) {
    st7565_command(CMD_SET_VOLUME_FIRST);
    st7565_command(CMD_SET_VOLUME_SECOND | (val & 0x3f));
}


void write_buffer(__xdata uint8_t *buff) {
  uint8_t c, p;

  for(p = 0; p < 8; p++) {

    st7565_command(CMD_SET_PAGE | pagemap[p]);
    st7565_command(CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    st7565_command(CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    st7565_command(CMD_RMW);
    
    for(c = 0; c < 128; c++) {
      //uart_putw_dec(c);
      //uart_send(buff[(128*p)+c]);
      st7565_data(buff[(p<<7)+c]);
    }
  }
}

void scroll_left(__xdata uint8_t *buff, uint8_t shift)
{
  __asm
    push ar7
    push ar6
    push ar5

    mov b,#8
scroll_left_loop1:    
    mov ar5,#127
    
scroll_left_loop2:    
    mov ar7,dpl
    mov ar6,dph

    inc dptr
    movx a,@dptr
    mov dpl,ar7
    mov dph,ar6
    movx @dptr,a
    inc dptr

    djnz ar5,scroll_left_loop2
    clr a
    movx @dptr,a
    inc dptr
    djnz b,scroll_left_loop1
  
    pop ar5
    pop ar6
    pop ar7
  __endasm;
}


void scroll_right(__xdata uint8_t *buff, uint8_t shift)
{
  __asm
    push ar5

    mov a,#4 // dptr += 64*128/8 , 0x0400
    add a,dph
    mov dph,a
    
    mov b,#8
scroll_right_loop1:    

    mov ar5,#127
scroll_right_loop2:
    mov a,dpl
    clr c
    subb a,#2 // dptr -= 2
    mov dpl,a
    mov a,dph
    subb a,#0
    mov dph,a   
     
    movx a,@dptr
    inc dptr
    movx @dptr,a

    djnz ar5,scroll_right_loop2
    
    inc dpl // decrement dptr
    djnz dpl,scroll_right_loop3
    dec dph

scroll_right_loop3:    
    dec dpl
    
    clr a
    movx @dptr,a

    djnz b,scroll_right_loop1
  
    pop ar5

  __endasm;
}

// shift[2:0] - step in pixels vert
// shift[3] - 0 - up, 1 - down
// shift[6:4] - step in pixels hor
// shift[7] - 0 - right, 1 - left- 
void scroll(__xdata uint8_t *buff, uint8_t shift) {
  uint8_t c, p, tmp1, tmp2, step_size = shift & 7;
  
  if(step_size)
  {
    if(shift & 0x08) // up
    {
      for(c = 0; c < 128; c++) {
        for(p = 0, tmp2 = 0; p < 8 ; p++) {
          tmp1 = buff[(p<<7)+c]<<(8-step_size); // Save LSB
          buff[(p<<7)+c] = (buff[(p<<7)+c]>>step_size) | tmp2;
          tmp2 = tmp1;
        }
      }  
    }
    else
    {
      for(c = 0; c < 128; c++) {
        p = 8; tmp2 = 0;
        while(p--) {
          tmp1 = buff[(p<<7)+c]>>(8-step_size); // Save MSB
          buff[(p<<7)+c] = (buff[(p<<7)+c]<<step_size) | tmp2;
          tmp2 = tmp1;
        }
      }
    }
  }

  step_size = ((shift>>4) & 7);
  if(step_size)
  {
    if(shift & 0x80) // left
    {
      for(p = 0; p < 8 ; p++) {
        for(c = step_size; c < 128; c++) {
          buff[(p<<7)+c-step_size] = buff[(p<<7)+c];
        }
        for(c = 128 - step_size; c < 128; c++) {
          buff[(p<<7)+c] = 0;
        }
      }  
    }
    else // right
    {
      for(p = 0; p < 8 ; p++) {
        for(c = 128 - step_size - 1; c != 255; c--) {
          buff[(p<<7)+c+step_size] = buff[(p<<7)+c];
        }
        for(c = 0; c < step_size; c++) {
          buff[(p<<7)+c] = 0;
        }
      }  
    }
  }  
  
}

void scroll_display(__xdata uint8_t *buff, uint8_t n, uint8_t shift) {
  uint8_t c;

  for(c = 0; c < n; c++) {
    scroll(buff, shift);
    write_buffer(buff);
    _delay_ms(20);
    
  }

}





#ifndef __UTIL_H__
#define __UTIL_H__

#include <8052.h>


void delay(unsigned int mstim);
unsigned char to_hex(unsigned char x);
unsigned int crc16(__xdata unsigned char *addr, unsigned int num, unsigned int crc);




#endif //__UTIL_H__

# README #

This is a firmware for my handmade mcs51-based board. It has a MCU connected to 128 kiB SRAM chip. Some code can be uploaded and executed in RAM.

## Building ##

It can be built using SDCC. Just run make.bat .
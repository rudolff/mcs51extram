#include <8052.h>
#include <string.h>
#include "util.h"
#include "uart.h"
#include "xram.h"
#include "main.h"

// Copy memory block in external ram
void on_copy() 
{
  xram_copy(uart_receive_uint(), uart_receive_uint(), uart_receive_uint(), uart_receive());  
  uart_send_string("Ok"); uart_send_newline();
}

// Receive data from UART
void on_load()
{
  __xdata unsigned char * dp = (__xdata unsigned char *) uart_receive_uint();
  unsigned int i, length = uart_receive_uint();
  unsigned int csum = uart_receive_uint(), calculated_csum = 0xFF;

  for(i = 0; i < length; i++)
  {
    dp[i] = uart_receive();
  }

  calculated_csum = crc16(dp, length, 0xFFFF);
  
  if(csum != calculated_csum)
  {
    uart_send_string("Checksum error, received: ");
    uart_send_hex((unsigned int) csum, 0);
    uart_send_string(", calculated: ");
    uart_send_hex((unsigned int) calculated_csum, 0);
    uart_send_newline();
    return;
  }
    
  uart_send_string("Ok");
  uart_send_newline();
}

// Fill memory with some pattern
void on_fill()
{
  __xdata unsigned char * dp = (__xdata unsigned char *) uart_receive_uint();
  unsigned int i, j, length = uart_receive_uint();
  unsigned char data_length = uart_receive();
  unsigned char buf[32];
  
  if(data_length > 32)
  {
    uart_send_string("Buffer size exceeded\n");
    return;
  }

  for(i=0; i<data_length; i++)
  {
    buf[i] = uart_receive();
  }  

  for(i=0, j=0; i<length; i++, j++)
  {
    if(j >= data_length)
    {
      j = 0;
    }
    dp[i] = buf[j];
  }
  uart_send_string("Ok"); uart_send_newline();
}

// Dump RAM to UART
void on_dump()
{
  __xdata unsigned char * dp = (__xdata unsigned char *) uart_receive_uint();
  unsigned int i = 0, length = uart_receive_uint();
  
  for(i=0; i<length; i++, dp++)
  {
    if(!(i & 15))
    {
      uart_send_newline();
      uart_send_hex((unsigned int) dp, 0);  
      uart_send(' ');   
    }        
  
    uart_send_hex((unsigned int) *dp, 1);
    uart_send(' ');
  }
  uart_send_newline();
  uart_send_string("Ok"); uart_send_newline();
}

// Dump ROM to UART
void on_dump_rom()
{
  __code unsigned char * cp = (__code unsigned char *) uart_receive_uint();
  unsigned int i = 0, length = uart_receive_uint();
  
  for(i=0; i<length; i++, cp++)
  {
    if(!(i & 15))
    {
      uart_send_newline();
      uart_send_hex((unsigned int) cp, 0);  
      uart_send(' ');   
    }        
  
    uart_send_hex((unsigned int) *cp, 1);
    uart_send(' ');
  }
  uart_send_newline();
  uart_send_string("Ok"); uart_send_newline();
}

// Jump to some address
void on_jump()
{
  unsigned int cp = uart_receive_uint();

  uart_send_string("Ok"); uart_send_newline();
  ((__code void (*)()) cp )(); 
}


#define MAKE_ISR(NUM) void isr_## NUM(void) __interrupt NUM __naked \
{ \
  __code unsigned char *cp = (__code unsigned char *) (XRAM_ROM_START + (NUM << 3) + 0x0003); \
  if(*cp == 0x02) /* 0x02: LJMP opcode */ \
  { \
    __asm ljmp (XRAM_ROM_START + (NUM << 3) + 0x0003) __endasm; \
  } \
} \

//MAKE_ISR(0)
MAKE_ISR(1)
MAKE_ISR(2)
MAKE_ISR(3)
MAKE_ISR(4)
MAKE_ISR(5)
MAKE_ISR(6)
MAKE_ISR(7)

void isr_0(void) __interrupt 0 __naked 
{ 
  __code unsigned char *cp = (__code unsigned char *) (XRAM_ROM_START); 
  if(*cp == 0x02) 
  { 
    ((__code void (*)()) cp )(); 
  } 
}

void on_test()
{
  unsigned char prev_page = xram_getpage();
  unsigned char i;

  for(i = 0; i < XRAM_PAGES_COUNT; i++)
  {
    xram_setpage(i);
    uart_send_string("Checking page ");
    uart_send(to_hex(i));
    uart_send_newline();
    xram_check();
  }
  
  uart_send_string("XRAM check done\n");
  xram_find_iram();
  xram_find_common_rom();
  
  xram_setpage(prev_page);
  uart_send_string("Ok"); uart_send_newline();
}

void on_xram()
{
  unsigned char enabled = uart_receive();

  xram_enabled(enabled);
  uart_send_string("Ok"); uart_send_newline();
}

//
void show_sections()
{
  unsigned char i;
  for(i=0; i<SECTIONS_COUNT; i++)
  {
    uart_send_hex((unsigned int) i, 1);
    uart_send('\t');
    
    switch(sections[i].type) 
    {
      case SECTION_TYPE_EMPTY:
        uart_send_string("EMPTY SLOT");
        uart_send_newline();
        continue;
        break;

      case SECTION_TYPE_CODE:
        uart_send_string("CODE");
        break;

      case SECTION_TYPE_DATA:
        uart_send_string("DATA");
        break;
        
      default:
        uart_send_string("UNDEFINED TYPE");
        uart_send_newline();
        continue;
        break;

    
    }
    uart_send('\t');
    uart_send_hex(sections[i].page, 1);
    uart_send('\t');
    uart_send_hex(sections[i].addr, 0);
    uart_send('\t');
    uart_send_hex(sections[i].length, 0);
    uart_send('\t');
    uart_send_hex(sections[i].checksum, 0);
    uart_send('\t');
    uart_send_limited_string(sections[i].name, SECTION_NAME_LENGTH);
    uart_send_newline();
    
  }
  uart_send_string("Ok"); uart_send_newline();
  
}

// Section it is a destriprion of a data portion, may be executable or not 
void init_sections() 
{
  memset (sections, 0, sizeof(section_info) * SECTIONS_COUNT);
}

// Receives external command and run it in loop
void main()
{
  xram_init();
  init_sections();
  uart_init();
  
  // turn on interrupts
  EX0 = 1;
  EA = 1;
  P1_5 = 0; // reset display

  uart_send_string("MCS51 board\n"); 
 
  while(1)
  { 
    unsigned char a;
    
    // Read sync sequence AA 55
    if((a = uart_receive()) != 0xAA)
    {
      continue;
    }
    if((a = uart_receive()) != 0x55)
    {
      continue;
    }   

    // Command
    a = uart_receive();
    switch(a)
    {
      case CMD_LOAD:
        on_load();
        break;

      case CMD_FILL:
        on_fill();
        break;
                
      case CMD_DUMP:
        on_dump();
        break;
        
      case CMD_DUMP_ROM:
        on_dump_rom();
        break; 
                       
      case CMD_JUMP:
        on_jump();     
        break;

      case CMD_COPY:
        on_copy();
        break;

      case CMD_SETPAGE:
        a = uart_receive();
        if(a&0x80)
        {
          uart_send_string("XRAM disabled");
          xram_enabled(0);
          uart_send_newline();
          uart_send_string("Ok"); uart_send_newline();
        }
        else
        {
          xram_enabled(1);
          uart_send_string("Old page: ");
          uart_send(to_hex(xram_getpage()));
          uart_send_newline();
          
          uart_send_string("New page: ");
          uart_send(to_hex(a));
          uart_send_newline(); 
          xram_setpage(a);       
          uart_send_string("Ok"); uart_send_newline();
        }
        break;
        
      case CMD_GETPAGE:
        a = uart_receive();
        uart_send_string("Current page: ");
        uart_send(to_hex(xram_getpage()));
        uart_send_newline();
        break;

      case CMD_ID:
        uart_send_string("MCS51 board\n");
        break;
        
      case CMD_TEST:
        on_test();
        break;  
        
      case CMD_XRAM:
        on_xram();
        break; 
        
      case CMD_XRAM_SECTIONS:
        show_sections();
        break;          
           
        
      default:
        uart_send_string("CMD error\n");
    }

    
  }

}





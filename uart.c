
#include "uart.h"

void uart_init()
{
  TMOD=0x20;      // timer1 8 bit auto reload mode
  TH1=UART_SPEED; //set 9600 baud rate, 19200 for 6T
  SCON=0x50;      //serial mode 1 with receive enable
	TR1  = 1;
	SBUF = '\n'; // Initialize TI flag
}


unsigned char uart_receive ( )
{
	while(!RI);
	RI = 0;
	return SBUF;
}

unsigned int uart_receive_uint ( )
{
  unsigned char high, low;
  low = uart_receive();
  high = uart_receive();
	return high << 8 | low;
}

void uart_send(unsigned char rec)
{
  while(!TI);
  TI=0;
  SBUF = rec;
}

void uart_send_string(unsigned char * str)
{
  while(*str)
  {
    uart_send(*str);
    str++;
  }
}

void uart_send_bytes(unsigned char * str, unsigned char size)
{
  while(size--)
  {
    uart_send(*str);
  }
}

void uart_send_limited_string(unsigned char * str, unsigned char size)
{
  while(*str && size--)
  {
    uart_send(*str);
  }
}


void uart_send_newline()
{
  uart_send('\r');
  uart_send('\n');
}


void uart_send_hex(unsigned int x, char one_byte)
{
  if(!one_byte)
  {
    uart_send(to_hex(x >> 12));
    uart_send(to_hex((x >> 8) & 0x0F));   
  }
  uart_send(to_hex((x >> 4) & 0x0F));            
  uart_send(to_hex(x & 0x0F));   
}


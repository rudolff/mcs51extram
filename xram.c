
#include "xram.h"


void xram_init()
{
  XRAM_PAGE = 1;
  XRAM_DISABLED = 0;
}


void xram_setpage(unsigned char num)
{
  XRAM_PAGE = num;  
}

void xram_enabled(unsigned char enabled)
{
  XRAM_DISABLED = !enabled;  
}

unsigned char xram_getpage()
{
  return (unsigned char)XRAM_PAGE;  
}

void xram_fill(unsigned int start, unsigned int size, unsigned char data)
{
  __xdata unsigned char * dp = (__xdata unsigned char *) start;
  unsigned int i=0;
    
  for(; i<size; i++, dp++)
  {
    *dp = data;
  }
}

// Finds upper bound of internal XRAM
void xram_find_iram()
{
  __xdata unsigned char * dp;
  unsigned char prev_page = xram_getpage();
   
  xram_setpage(0);  
  xram_fill((unsigned int)XRAM_START, (unsigned int)~XRAM_START, 0x5A);  

  xram_setpage(1);  
  xram_fill((unsigned int)XRAM_START, (unsigned int)~XRAM_START, 0xFF);  

  xram_setpage(0);
    
  for(dp = (__xdata unsigned char *) XRAM_START; dp != 0; dp++)
  {
    if(*dp != 0xFF)
    {
      uart_send_string("XRAM: Found difference at: ");
      uart_send_hex((unsigned int) dp, 0);  
      uart_send_newline();
      return;
    }
  }
  
  xram_setpage(prev_page); 
}

void xram_find_common_rom()
{
  __code unsigned char * cp;
  unsigned char found = 0;
  unsigned char prev_page = xram_getpage();
   
  xram_setpage(0);  
  xram_fill((unsigned int)XRAM_ROM_START, (unsigned int)~XRAM_ROM_START, 0x5A);  

  xram_setpage(1);  
  xram_fill((unsigned int)XRAM_ROM_START, (unsigned int)~XRAM_ROM_START, 0xA5);  

    
  for(cp = (__code unsigned char *) XRAM_ROM_START; cp != 0; cp++)
  {
    if(*cp == 0x5A && !found)
    {
      found = 1;
      uart_send_string("XROM: Found common rom start at: ");
      uart_send_hex((unsigned int) cp, 0);  
      uart_send_newline();      
    }
    
    if(*cp != 0x5A && found)
    {
      found = 0;
      uart_send_string("XROM: Found common rom end at: ");
      uart_send_hex((unsigned int) cp, 0);  
      uart_send_newline();      
    }    
    
  }
  
  xram_setpage(prev_page); 
}

// Tests XRAM
void xram_check()
{
  __xdata unsigned char * dp;
  __code unsigned char * cp = (__code unsigned char *) XRAM_ROM_START;
    
  for(dp = (__xdata unsigned char *) XRAM_START; dp != 0; dp++)
  {
    *dp = 0;
  }
  
  for(dp = (__xdata unsigned char *) XRAM_START; dp != 0; dp++)
  {
    if(*dp != 0)
    {
      uart_send_string("XRAM: Filling zero error at: ");
      uart_send_hex((unsigned int) dp, 0);  
      uart_send_newline();
      return;
    }
    *dp = 0xFF;
  }
  
  for(dp = (__xdata unsigned char *) XRAM_START; dp != 0; dp++)
  {
    if(*dp != 0xFF)
    {
      uart_send_string("XRAM: Filling ones error at: ");
      uart_send_hex((unsigned int) dp, 0);  
      uart_send_newline();
      return;
    }
    *dp = 0x5A;
  }

  for(dp = (__xdata unsigned char *) XRAM_START; dp != 0; dp++)
  {
    if(*dp != 0x5A)
    {
      uart_send_string("XRAM: Filling 0x5A error at: ");
      uart_send_hex((unsigned int) dp, 0);  
      uart_send_newline();
      return;
    }
    *dp = 0xA5;
  }
  
  if(xram_getpage() == XRAM_COMMON_ROM_PAGE)
  {
    for(; cp != 0; cp++)
    {
      if(*cp != 0xA5)
      {
        uart_send_string("XRAM: Reading code error at: ");
        uart_send_hex((unsigned int) cp, 0);  
        uart_send_newline();
        return;
      }
    }
  }

}

// Buffer size is a power of 2
#define XRAM_BUF_BITS 4
#define XRAM_BUF_SIZE (1<<XRAM_BUF_BITS)
#define XRAM_BUF_MASK (XRAM_BUF_SIZE-1)

// Copy between different pages of memory
// pages upper nibble is a src page, lower nibble is a dest page
void xram_copy(unsigned int src, unsigned int dst, unsigned int size, unsigned char pages)
{
  unsigned char src_page = pages >> 4;
  unsigned char dst_page = pages & 0xF;
  unsigned char prev_page;
  __idata unsigned char buf[XRAM_BUF_SIZE];
  unsigned int i, j, n;
  
  __xdata unsigned char * srcp = (__xdata unsigned char *) src;
  __xdata unsigned char * dstp = (__xdata unsigned char *) dst;
  
  if(src >= XRAM_ROM_START && src_page == XRAM_COMMON_ROM_PAGE)
  {
    xram_copy_rom_to_ram(src, dst, size, dst_page);
  }
  else if(src_page == dst_page) // copy inside one page
  {  
    if(src > dst)
    {
      prev_page = xram_getpage();
      
      xram_setpage(dst_page);
      for(i=0; i<size; i++, srcp++, dstp++)
      {
        *dstp = *srcp;
      }
      
      xram_setpage(prev_page);
    }
    else if(src < dst)
    {
      prev_page = xram_getpage();
      
      srcp = (__xdata unsigned char *) src + size - 1;
      dstp = (__xdata unsigned char *) dst + size - 1;
      
      xram_setpage(dst_page);
      for(i=0; i<size; i++, srcp--, dstp--)
      {
        *dstp = *srcp;
      }
      
      xram_setpage(prev_page);
    }    
  }
  else // copy between different pages
  {
    prev_page = xram_getpage();
  
    for(j=0, n=size >> XRAM_BUF_BITS; j<n; j++)
    {
    
      xram_setpage(src_page);
      for(i=0; i<XRAM_BUF_SIZE; i++, srcp++)
      {
        buf[i] = *srcp;
      }
      
      xram_setpage(dst_page);  
      for(i=0; i<XRAM_BUF_SIZE; i++, dstp++)
      {
        *dstp = buf[i];
      }  
    }
    
    xram_setpage(src_page);
    for(i=0, n=size & XRAM_BUF_MASK; i<n; i++, srcp++)
    {
      buf[i] = *srcp;
    }
    
    xram_setpage(dst_page);  
    for(i=0, n=size & XRAM_BUF_MASK; i<n; i++, dstp++)
    {
      *dstp = buf[i];
    } 
    
    xram_setpage(prev_page);
  }

}

void xram_copy_rom_to_ram(unsigned int src, unsigned int dst, unsigned int size, unsigned char dst_page)
{
  unsigned char prev_page;
  unsigned int i;
  
  __code unsigned char * srcp = (__code unsigned char *) src;
  __xdata unsigned char * dstp = (__xdata unsigned char *) dst;
  
  if(dst_page == XRAM_COMMON_ROM_PAGE)
  {  
    if(src > dst)
    {
      prev_page = xram_getpage();
      
      xram_setpage(dst_page);
      for(i=0; i<size; i++, srcp++, dstp++)
      {
        *dstp = *srcp;
      }
      
      xram_setpage(prev_page);
    }
    else if(src < dst)
    {
      prev_page = xram_getpage();
      
      srcp = (__code unsigned char *) src + size - 1;
      dstp = (__xdata unsigned char *) dst + size - 1;
      
      xram_setpage(dst_page);
      for(i=0; i<size; i++, srcp--, dstp--)
      {
        *dstp = *srcp;
      }
      
      xram_setpage(prev_page);
    }    
  }
  else
  {
    prev_page = xram_getpage();
    
    xram_setpage(dst_page);
    for(i=0; i<size; i++, srcp++, dstp++)
    {
      *dstp = *srcp;
    }
    
    xram_setpage(prev_page);
  }
}


#ifndef __UART_H__
#define __UART_H__

#include <8052.h>
#include "util.h"

#define UART_19200 250
#define UART_38400 253
#define UART_57600 254
#define UART_115200 255

#define UART_SPEED UART_115200



void uart_init();
unsigned char uart_receive ( );
unsigned int uart_receive_uint ( );
void uart_send(unsigned char rec);
void uart_send_string(unsigned char * str);
void uart_send_limited_string(unsigned char * str, unsigned char size);
void uart_send_bytes(unsigned char * str, unsigned char size);
void uart_send_newline();
void uart_send_hex(unsigned int x, char one_byte);


#endif //__UART_H__
